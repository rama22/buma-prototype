package com.buma.sentinel.services;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.springframework.stereotype.Service;

/**
 * FileUploadServices
 */
@Service
public class FileUploadServices {

	public String doUploadBase64(String basePath, String base64File, String fileName) {
		// Create directory
		new File(basePath).mkdirs();

		String[] strings = base64File.split(",");
        String extension;
        switch (strings[0]) {//check image's extension
            case "data:image/jpeg;base64":
                extension = ".jpeg";
                break;
            case "data:image/png;base64":
                extension = ".png";
                break;
            default://should write cases for more images types
                extension = ".jpg";
                break;
        }
        //convert base64 string to binary data
        byte[] data = DatatypeConverter.parseBase64Binary(strings[1]);
        String img = basePath + fileName + extension;
        File file = new File(img);
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
            outputStream.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return "false";
		
	}
}