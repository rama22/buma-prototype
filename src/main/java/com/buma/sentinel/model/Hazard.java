package com.buma.sentinel.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Hazard
 */
@Entity
@Table(name = "dsis_hse_hazard_isafe")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
@Getter
@Setter 
@NoArgsConstructor
public class Hazard implements Serializable {
	public Hazard(String title, String description, String detail_loc, String location, String ktadantta,
			String categories, String pelapor) {
				this.title = title;
				this.description = description;
				this.detail_loc = detail_loc;
				this.location = location;
				this.ktadantta = ktadantta;
				this.categories = categories;
				this.pelapor = pelapor;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Column(name = "title")
	private String title;

	@NotBlank
	@Column(name = "description")
	private String description;

	@NotBlank
	@Column(name = "detail_loc")
	private String detail_loc;

	@NotBlank
	@Column(name = "location")
	private String location;

	@NotBlank
	@Column(name = "ktadantta")
	private String ktadantta;

	@NotBlank
	@Column(name = "categories")
	private String categories;

	@NotBlank
	@Column(name = "pelapor")
	private String pelapor;

	@Column(nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date createdAt;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date updatedAt;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "hazard_terlibat",
		joinColumns = @JoinColumn(name = "user_id"),
		inverseJoinColumns = @JoinColumn(name = "hazard_id"))
	private Set<User> terlibat = new HashSet<>();
	
}