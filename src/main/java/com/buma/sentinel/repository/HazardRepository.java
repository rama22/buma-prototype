package com.buma.sentinel.repository;

import com.buma.sentinel.model.Hazard;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * HazardRepository
 */
@Repository
public interface HazardRepository extends JpaRepository<Hazard, Long> {

	
}