package com.buma.sentinel.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import com.buma.sentinel.exception.ResourceNotFoundException;
import com.buma.sentinel.message.request.HazardForm;
import com.buma.sentinel.model.Hazard;
import com.buma.sentinel.model.User;
import com.buma.sentinel.repository.HazardRepository;
import com.buma.sentinel.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * HazardController
 */
@RestController
@RequestMapping("/api")
// @PreAuthorize("hasRole('USER') or hasRole('ADMIN') or hasRole('PM')")
public class HazardController {

	@Autowired
	HazardRepository hazardRepository;

	@Autowired
	UserRepository userRepository;

	// Get All Hazard
	@GetMapping("/hazard")
	public List<Hazard> getAllHazard() {
		return hazardRepository.findAll();
	}

	// Create a new Hazard
	@PostMapping("/hazard")
	public Hazard createHazard(@Valid @RequestBody HazardForm hazardForm) {
		// Creating hazard
        Hazard hazard = new Hazard(hazardForm.getTitle(), hazardForm.getDescription(),
				hazardForm.getDetail_loc(), hazardForm.getLocation(), hazardForm.getKtadantta(), hazardForm.getCategories(), hazardForm.getPelapor());
				
		Set<String> strTerlibat = hazardForm.getTerlibat();
		Set<User> terlibats = new HashSet<>();

		strTerlibat.forEach(terlibat -> {
			User userTerlibat = userRepository.findByUsername(terlibat)
				  .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User not find."));
			terlibats.add(userTerlibat);
		});

		hazard.setTerlibat(terlibats);
		return hazardRepository.save(hazard);
	}

	// Get a Single Hazard
	@GetMapping("/hazard/{id}")
	public Hazard getHazardById(@PathVariable(value = "id") Long hazardId) {
		return hazardRepository.findById(hazardId)
				.orElseThrow(() -> new ResourceNotFoundException("Hazard", "id", hazardId));
	}

	// Update a hazard
	@PutMapping("/hazard/{id}")
	public Hazard updateHazard(@PathVariable(value = "id") Long hazardId, @Valid @RequestBody HazardForm hazardDetails) {
		Hazard hazard = hazardRepository.findById(hazardId)
			.orElseThrow(() -> new ResourceNotFoundException("Hazard", "id", hazardId));

		hazard.setTitle(hazardDetails.getTitle());
		hazard.setDescription(hazardDetails.getDescription());
		hazard.setDetail_loc(hazardDetails.getDetail_loc());
		hazard.setLocation(hazardDetails.getLocation());
		hazard.setKtadantta(hazardDetails.getKtadantta());
		hazard.setCategories(hazardDetails.getCategories());
		hazard.setPelapor(hazardDetails.getPelapor());

		Set<String> strTerlibat = hazardDetails.getTerlibat();
		Set<User> terlibats = new HashSet<>();

		strTerlibat.forEach(terlibat -> {
			User userTerlibat = userRepository.findByUsername(terlibat)
				  .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User not find."));
			terlibats.add(userTerlibat);
		});

		hazard.setTerlibat(terlibats);

		return hazardRepository.save(hazard);
	}

	// Delete a Hazard
	@DeleteMapping("/hazard/{id}")
	public ResponseEntity<?> deleteHazard(@PathVariable(value = "id") Long hazardId) {
		Hazard hazard = hazardRepository.findById(hazardId)
			.orElseThrow(() -> new ResourceNotFoundException("Hazard", "id", hazardId));

		hazardRepository.delete(hazard);

		return ResponseEntity.ok().build();
		
	}
}