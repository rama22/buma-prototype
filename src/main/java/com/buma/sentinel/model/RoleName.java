package com.buma.sentinel.model;

/**
 * RoleName
 */
public enum RoleName {
	ROLE_USER,
	ROLE_PM,
	ROLE_ADMIN
}