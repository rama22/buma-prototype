package com.buma.sentinel.repository;

import java.util.Optional;

import com.buma.sentinel.model.Role;
import com.buma.sentinel.model.RoleName;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * RoleRepository
 */
@Service
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(RoleName roleName);
	
}