package com.buma.sentinel.message.request;

import java.util.Set;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * HazardForm
 */
@Getter
@Setter 
@NoArgsConstructor
public class HazardForm {

	@NotBlank
	@Column(name = "title")
	private String title;

	@NotBlank
	@Column(name = "description")
	private String description;

	@NotBlank
	@Column(name = "detail_loc")
	private String detail_loc;

	@NotBlank
	@Column(name = "location")
	private String location;

	@NotBlank
	@Column(name = "ktadantta")
	private String ktadantta;

	@NotBlank
	@Column(name = "categories")
	private String categories;

	@NotBlank
	@Column(name = "pelapor")
	private String pelapor;

	private Set<String> terlibat;

	public Set<String> getTerlibat() {
		return this.terlibat;
	  }
	  
	  public void setTerlibat(Set<String> terlibat) {
		this.terlibat = terlibat;
	  }
}